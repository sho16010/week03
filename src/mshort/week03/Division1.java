package mshort.week03;

import java.util.*;
import java.lang.System;

public class Division1 {

    public static void main(String[] args) {
        int firstNum = 0, secondNum = 0;
        float divResult;
        Scanner myScanner = new Scanner(System.in);

        System.out.println("Division1 Program (Exception Handling)");
        System.out.println("You will be prompted to enter two numbers. Press Enter after entering each number.");
        System.out.println();

        /* Gather Input of two numbers from the keyboard in main method */
        try {
            System.out.print("First  Number: ");
            firstNum = myScanner.nextInt();
            System.out.print("Second Number: ");
            secondNum = myScanner.nextInt();
        } catch (InputMismatchException x) {
            System.out.println("Error: Please Provide Numbers Only For Input!");
            System.out.println("Exiting...");
            /* Remember to close myScanner */
            myScanner.close();
            /* Bail.. You have to put in good numbers to start */
            System.exit(1);
        }


        /* Call doDivision method to calculate */
        try {
            divResult = doDivision(firstNum, secondNum);
            System.out.println("Answer is: " + divResult);
        } catch (ArithmeticException e) {
            System.out.println("Exception Encountered: [" + e.getMessage() + "]");
            System.out.println();
            System.out.println("Please Try Again...");
            System.out.println();

            System.out.print("First  Number: ");
            firstNum = myScanner.nextInt();
            System.out.print("Second Number: ");
            secondNum = myScanner.nextInt();

            try {
                divResult = doDivision(firstNum, secondNum);
                System.out.println("Answer is: " + divResult);
            } catch (ArithmeticException f) {
                System.out.println("Exception Encountered during Recovery Attempt: [" + f.getMessage() + "]");
                System.out.println("Giving Up...");
            }
        } finally {
            System.out.println();
            System.out.println("This message was brought to you by the \"finally\" statement.");
            /* Remember to close myScanner */
            myScanner.close();
        }
    }

    /* Write a method that divides the two numbers together (num1/num2) and returns the result. The method should
     include a throws clause for the most specific exception possible if the user enters zero for num2. */
    static float doDivision(int num1, int num2) throws ArithmeticException {
        float result;

        result = num1 / num2;

        return result;
    }

}
