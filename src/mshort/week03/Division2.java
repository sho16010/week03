package mshort.week03;

import java.util.*;

public class Division2 {

    public static void main(String[] args) {
        int firstNum = 0, secondNum = 0;
        float divResult;
        Scanner myScanner = new Scanner(System.in);

        System.out.println("Division2 Program (Data Validation on Input)");
        System.out.println("You will be prompted to enter two numbers. Press Enter after entering each number.");
        System.out.println();

        /* Gather Input of two numbers from the keyboard in main method */
        boolean validNum1 = false, validNum2 = false;
        while (!validNum1) {
            System.out.print("First  Number: ");
            String n1 = myScanner.next();
            if (n1.matches("^-?[0-9]+$")) {
                firstNum = Integer.parseInt(n1);
                validNum1 = true;
            } else {
                System.out.println("Error: Invalid Input for Number, Please try Again!");
            }
        }
        while (!validNum2) {
            System.out.print("Second Number: ");
            String n2 = myScanner.next();
            if (n2.matches("^-?[0-9]+$")) {
                secondNum = Integer.parseInt(n2);
                if (secondNum == 0) {
                    System.out.println("Error: Cannot Divide by Zero, Please Enter Another Number!");
                } else {
                    validNum2 = true;
                }
            } else {
                System.out.println("Error: Invalid Input for Number, Please try Again!");
            }
        }

        /* Call doDivision method to calculate */
        divResult = doDivision(firstNum, secondNum);
        System.out.println("Answer is: " + divResult);
        myScanner.close();
    }

    /* Write a method that divides the two numbers together (num1/num2) and returns the result. The method should
     include a throws clause for the most specific exception possible if the user enters zero for num2. */
    static float doDivision(int num1, int num2) throws ArithmeticException {
        float result;

        result = num1 / num2;

        return result;
    }

}
